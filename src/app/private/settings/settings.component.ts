import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ApiParametersService } from '@app/services/api-parameters.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  githubSettingsFormGroup: FormGroup;
  settingsformincomplete = false;
  useApiV4: boolean;
  token: string;

  constructor(private _formBuilder: FormBuilder, private apiInfo: ApiParametersService) {
    if (apiInfo.getToken()) {
      this.token = apiInfo.getToken();
    }
    if (apiInfo.getV4info()) {
      this.useApiV4 = apiInfo.getV4info();
    }
  }

  ngOnInit() {
    this.githubSettingsFormGroup = this._formBuilder.group({
      tokenFormControl: ['', Validators.required],
      v4FormControl: ['']
    });
  }

  saveSettings() {
    this.apiInfo.saveInformations(this.token, this.useApiV4);
  }
}
