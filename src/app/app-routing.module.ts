import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { HomeComponent } from './public/home/home.component';
import { extract } from './core';
import { AboutComponent } from './public/about/about.component';
import { SettingsComponent } from './private/settings/settings.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, data: { title: extract('Home') } },
    { path: 'about', component: AboutComponent, data: { title: extract('About') } }
  ]),
  Shell.childRoutesSecure([{ path: 'settings', component: SettingsComponent, data: { title: extract('Settings') } }]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
