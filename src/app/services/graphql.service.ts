import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-angular-link-http';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import gql from 'graphql-tag';
import { environment } from '@env/environment';
import { CredentialsService } from '@app/core';
import { map } from 'rxjs/operators';
import { ApiParametersService } from './api-parameters.service';

@Injectable({
  providedIn: 'root'
})
export class GraphqlService {
  token: string;

  constructor(private apollo: Apollo, private apiInfo: ApiParametersService) {
    this.token = apiInfo.getToken();

    const httpLink = createHttpLink({
      uri: environment.gitlabUrl + 'graphql'
    });

    const authLink = setContext((_: any, { headers }: any) => {
      return {
        headers: {
          ...headers,
          Authorization: 'Bearer  ' + this.token
        }
      };
    });

    this.apollo.create({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache()
    });
  }

  public getUser = (username: string) => {
    return this.apollo.query({
      query: gql`
        query {
          user(login: "nikme") {
            id
            login
            bio
            email
            name
            avatarUrl
            createdAt
          }
        }
      `
    });
  };

  public getUserRepo = (username: string) => {
    return this.apollo.query({
      query: gql`
        query {
          user(login: "${username}") {
            id
            repositories(privacy: PUBLIC, first: 10) {
              edges {
                node {
                  id
                  name
                  isPrivate
                  createdAt
                  description
                  updatedAt
                }
              }
              totalCount
            }
          }
        }
        `
    });
  };

  public searchRepo = (searchword: string) => {
    return this.apollo.query({
      query: gql`
        query {
          search(query: "${searchword}", type: REPOSITORY, first: 10) {
            repositoryCount
            edges {
              node {
                ... on Repository {
                  id
                  name
                  isPrivate
                  createdAt
                  description
                  updatedAt
                }
              }
            }
          }
        }
        `
    });
  };
}
