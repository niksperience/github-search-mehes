import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiParametersService {
  useApiV4: boolean;
  token: string;
  constructor() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
    }
    if (localStorage.getItem('useV4')) {
      this.useApiV4 = JSON.parse(localStorage.getItem('useV4'));
    }
  }

  public saveInformations(token: string, useV4: boolean) {
    localStorage.setItem('token', token);
    localStorage.setItem('useV4', useV4.toString());
  }

  public getV4info() {
    return this.useApiV4;
  }

  public getToken() {
    return this.token;
  }
}
