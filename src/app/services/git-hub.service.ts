import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RestService } from './rest.service';
import { GraphqlService } from './graphql.service';
import { ApiParametersService } from './api-parameters.service';

@Injectable({
  providedIn: 'root'
})
export class GitHubService {
  useApiV4: boolean;

  constructor(
    private restService: RestService,
    private graphqlService: GraphqlService,
    private apiInfo: ApiParametersService
  ) {
    if (apiInfo.getV4info()) {
      this.useApiV4 = apiInfo.getV4info();
    }
  }

  getUser(username: string): Observable<any> {
    if (this.useApiV4) {
      return this.graphqlService.getUser(username).pipe(
        map((response: any) => {
          return response.data.user;
        })
      );
    } else {
      return this.restService.getUser(username).pipe(
        map((response: any) => {
          return response;
        })
      );
    }
  }

  getUserRepo(username: string) {
    if (this.useApiV4) {
      return this.graphqlService.getUserRepo(username).pipe(
        map((response: any) => {
          const temp: any[] = [];
          response.data.user.repositories.edges.forEach((element: any) => {
            temp.push(element.node);
          });
          return temp;
        })
      );
    } else {
      return this.restService.getUserRepo(username).pipe(
        map((response: any) => {
          return response;
        })
      );
    }
  }

  searchRepo(searchword: string) {
    if (this.useApiV4) {
      return this.graphqlService.searchRepo(searchword).pipe(
        map((response: any) => {
          const temp: any[] = [];
          response.data.search.edges.forEach((element: any) => {
            temp.push(element.node);
          });
          return temp;
        })
      );
    } else {
      return this.restService.searchRepo(searchword).pipe(
        map((response: any) => {
          return response.items;
        })
      );
    }
  }
}
