import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GitHubService } from '@app/services/git-hub.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

// tslint:disable: max-line-length
export class HomeComponent implements OnInit {
  quote: string | undefined;
  isLoading = false;
  element = 'none';
  githubuser: any = [];
  githubrepo: any = [];
  githubrepo_filter: any = [];
  githubUserFormGroup: FormGroup;
  githubRepoFormGroup: FormGroup;
  userformincomplete = false;
  repoformincomplete = false;
  userSearch = true;

  constructor(
    private _formBuilder: FormBuilder,
    private apiService: GitHubService,
    private changeDetection: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.githubUserFormGroup = this._formBuilder.group({
      usernameFormControl: ['', Validators.required],
      tokenFormControl: [''],
      v4FormControl: ['']
    });
    this.githubRepoFormGroup = this._formBuilder.group({
      repoFormControl: ['', Validators.required]
    });
  }

  setClass(elementID: string) {
    this.element = elementID;
  }

  getUserGitHub() {
    if (this.githubUserFormGroup.valid) {
      this.isLoading = true;
      this.apiService.getUser(this.githubUserFormGroup.controls.usernameFormControl.value).subscribe({
        next: (data: any) => {
          this.githubuser = data;
        },
        error: (data: any) => {
          console.log('Error', data);
        },
        complete: () => {}
      });
    } else {
      this.userformincomplete = true;
    }
  }

  getUserGitHubRepo() {
    this.isLoading = true;
    this.userSearch = false;
    this.apiService.getUserRepo(this.githubUserFormGroup.controls.usernameFormControl.value).subscribe({
      next: (data: any) => {
        this.githubrepo = data;
        this.githubrepo_filter = _.cloneDeep(data);
        this.changeDetection.detectChanges();
      },
      error: (data: any) => {
        console.log('Error', data);
      },
      complete: () => {}
    });
  }

  searchGitHub() {
    this.isLoading = true;
    if (this.githubRepoFormGroup.valid) {
      this.apiService.searchRepo(this.githubRepoFormGroup.controls.repoFormControl.value).subscribe({
        next: (data: any) => {
          this.githubrepo = data;
          this.githubrepo_filter = _.cloneDeep(data);
          this.changeDetection.detectChanges();
        },
        error: (data: any) => {
          console.log('Error', data);
        },
        complete: () => {}
      });
    } else {
      this.repoformincomplete = true;
    }
  }

  returnToUserSearch() {
    this.githubuser = {};
    this.githubUserFormGroup.controls.usernameFormControl.setValue('');
  }

  filterFetched() {
    const app: any = this;
    this.githubrepo_filter = this.githubrepo.filter(filterArray);
    function filterArray(element: any, index: number, array: []) {
      const searchword = app.githubRepoFormGroup.controls.repoFormControl.value.toString().toLowerCase();
      if (element.id) {
        const id = element.id.toString().toLowerCase();
        if (id.includes(searchword)) {
          return true;
        }
      }
      if (element.name) {
        const name = element.name.toString().toLowerCase();
        if (name.includes(searchword)) {
          return true;
        }
      }
      if (element.description) {
        const description = element.description.toString().toLowerCase();
        if (description.includes(searchword)) {
          return true;
        }
      }
      return false;
    }
  }
}
